package exo17;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
public class Exo17 {

	public static void main(String[] args) {
		
		
		List<Person> persons= new ArrayList<>();
		Person p1 = new Person ("Mahrez","Riyad",30);
		Person p2 = new Person ("Belmadi","Djamel",35);
		Person p3 = new Person ("Zineddine","Ziden",25);
		Person p4 = new Person ("Slimani","Islam",32);
		Person p5 = new Person ("Ziani","Karim",38);
		Person tab[]= {p1,p2,p3,p4,p5};
		for(int i=0;i<tab.length;i++) {
			persons.add(tab[i]);
		}
		String file = "files/binfile1.bin";
		try(PersonOutputStream personOutputStream=new PersonOutputStream(new FileOutputStream(file))){
			personOutputStream.writeFields(persons);
			System.out.println(" voir le fichier 'binfile2.bin' \n");
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("\n la 3eme Question: \n");
        try (PersonInputStream personInput = new PersonInputStream(new FileInputStream(file))) {
            List<Person> personne;
            personne = personInput.readFields();
            personne.forEach(System.out::println);
        } catch (IOException e) {
			e.printStackTrace();
		}
		

	}

}
