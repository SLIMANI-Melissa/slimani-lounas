
package exo18;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Exo18 {
	public static void main(String[] args) {
		   System.out.println("\n Question 1 et 2: \n");
		System.out.println(" ObjectOutputStream sert � �crire des objects directement dans un .bin et on doit implementer l'interface Serializable  \n");
		
		List<Person> perso= new ArrayList<>();
		Person p1 = new Person ("Belmadi","djamel",45);
		Person p2 = new Person ("Mahrez","riyad",30);
		Person p3 = new Person ("Slimani","Islem",32);
		Person p4 = new Person ("Zineddine","Ziden",40);
		Person p5 = new Person ("Casillas","Iker",45);
		Person tab[]= {p1,p2,p3,p4,p5};
		for(int i=0;i<tab.length;i++) {
			perso.add(tab[i]);
		}
        String fileName = "files/binfile3.bin";

        System.out.println("\n Question 3: \n");
        System.out.println("voir le fichier 'binfile4.bin' \n");
        try (PersonOutputStream personOutputStream = new PersonOutputStream(new FileOutputStream(fileName))) {
            personOutputStream.writePeople(perso);
        } catch (IOException e) {
            e.printStackTrace();
        }


        System.out.println("\n Question 4: \n");
        try (PersonInputStream personInputStream = new PersonInputStream(new FileInputStream(fileName))) {
            List<Person> people = personInputStream.readPeople();
            people.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
}

