package exo16;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class PersonReader extends Exo16 {

	 
	   public List<Person> read(String fileName) {

	  
	      File textPersons = new File(fileName);
	    
	  try (Reader reader = new FileReader(textPersons);
	            
	 BufferedReader bufferedReader = new BufferedReader(reader)) {
	            Stream<String> strings = bufferedReader.lines();
	            Stream<String> lignesSansComentaire= strings.filter(ligne -> !ligne.startsWith("#"));
	            
	    return lignesSansComentaire.flatMap(ligneToPer -> {
	                        try {
	                            return Stream.of(stringToPerson.apply(ligneToPer));
	                        } catch (Exception e) {
	                            e.printStackTrace();
	                        }
	                        return Stream.empty();
	                    })
	                    .collect(Collectors.toList());

	        } catch (IOException ioe) {
	            ioe.printStackTrace();
	        }

	        return Collections.emptyList();

	    }

	}
