package exo16;


import java.util.List;
import java.util.function.Function;

public class Exo16 {

	
    Function<String, Person> stringToPerson =
    		line -> {String[] mots = line.split(", ");String lastName = mots[0];String firstName = mots[1]; int age = Integer.parseInt(mots[2]);
        return new Person(firstName, lastName, age);
    };

   Function<Person, String> personToString =
		   person -> person.getLastName() + ", " +person.getFirstName() + ", " +person.getAge();

  
  public static void  main(String[] args) {
 
       String fileName = "files/fileName.txt";
   
     String WriterfileName = "files/WriterfileName";
    
    PersonReader reader = new PersonReader();
      
  PersonWriter writer = new PersonWriter();
      
  List<Person> persons = reader.read(fileName);
       
 System.out.println("la liste des personnes: ");
    
    persons.forEach(System.out::println);
     writer.write(persons,WriterfileName);

    }

}



